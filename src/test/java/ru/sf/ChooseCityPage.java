package ru.sf;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class ChooseCityPage {

    public final WebDriver webDriver;
    private static final String SEARCH_FIELD_CLASS = "locality-selector-popup__search-input";
    private static final String ERROR_MESSAGE_SPAN_CLASS = "locality-selector-popup__table-empty-text";
    private static final String URL = "https://dodopizza.ru/";

    public ChooseCityPage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public void go() {
        webDriver.get(URL);
    }

    public void searchCity(String city) {
        webDriver.findElement(By.className(SEARCH_FIELD_CLASS)).sendKeys(city, Keys.ENTER);
    }

    public String getCityNotFoundMessage() {
        return webDriver.findElement(By.className(ERROR_MESSAGE_SPAN_CLASS)).getText();
    }

}

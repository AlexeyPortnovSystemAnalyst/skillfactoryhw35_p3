package ru.sf;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

import static org.junit.Assert.assertEquals;


/**
 * Варинаты запуска сценариев:
 * Команда 'mvn clean test' в консоли
 * Через UI intellij IDEA
 */
public class StepDefinitions {

    public static final WebDriver webDriver;
    public static final ChooseCityPage CHOOSE_CITY_PAGE_OLD;
    public static final CityMenuPage cityMenuPage;

    //Процесс инициализации необходимых ресурсов
    static {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\89672\\OneDrive\\Документы\\projects\\QAJA_m35_cc-scenario\\src\\test\\resources\\chromedriver.exe");
        webDriver = new ChromeDriver();
        webDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        webDriver.manage().window().maximize();
        CHOOSE_CITY_PAGE_OLD = new ChooseCityPage(webDriver);
        cityMenuPage = new CityMenuPage(webDriver);
    }

    //Реализация шага
    //Текст должен строго соответствоать тексту сценария
    @Given("url of restaurant {string}")
    public void url_of_restaurant(String url) {
        CHOOSE_CITY_PAGE_OLD.go();
    }

    //Реализация шага
    @Then("chose city {string}")
    public void chose_city(String city) {
        CHOOSE_CITY_PAGE_OLD.searchCity(city);
    }

    //Реализация шага
    @Then("assert that chosen city is {string}")
    public void assert_that_chosen_city_is(String expectedCity) {
        //final var currentActiveCity = cityMenuPage.getCurrentActiveCity();
        assertEquals(expectedCity, cityMenuPage.getCurrentActiveCity());
    }

    //Реализация шага
    @Then("assert that user got message {string}")
    public void assert_that_user_got_message(String errorMessage) {
        //final var cityNotFoundMessage = CHOOSE_CITY_PAGE_OLD.getCityNotFoundMessage();
        assertEquals(errorMessage, CHOOSE_CITY_PAGE_OLD.getCityNotFoundMessage());
        //assertEquals(errorMessage, cityNotFoundMessage););
    }
}
